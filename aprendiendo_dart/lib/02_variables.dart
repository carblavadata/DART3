void main(List<String> args) {

  // Variables de tipo INT
  int entero = 1;
  // print(entero);

  entero = 5;
  // print(entero);

  //Variables de tipo DOUBLE

  double decimal = 1;
  // print(decimal);

  decimal = 5;
  // print(decimal);

  // Varables de tipo NUM, es el padre de int y de double.
  num number = 1.2;
  print(number);

  number = 5;
  print(number); 
}