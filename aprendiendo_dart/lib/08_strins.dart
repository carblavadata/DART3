void main(List<String> args) {
  String nombre = 'Carlos';
  print(nombre);

  int longitud_nombre = nombre.length;
  print(longitud_nombre);

  bool contains_Ca = nombre.contains('Ca');
  print('Contiene Ca? $contains_Ca');

  bool contains_ca_minuscula = nombre.contains('ca');
  print('Contiene ca minuscula? $contains_ca_minuscula');

  bool terminaConOS = nombre.endsWith('OS');
  bool terminaCon_os = nombre.endsWith('os');
  print('Termina con OS? $terminaConOS');
  print('Termina con os? $terminaCon_os');
  
  bool empiezaConMayuscula = nombre.startsWith('C');
  bool empiezaConMinuscula = nombre.startsWith('c');
  print('Empieza con mayuscula? $empiezaConMayuscula');
  print('Empieza con minuscula? $empiezaConMinuscula');

  String reemplazo = nombre.replaceAll('a', '');
  print(reemplazo);
}